
#include <SPI.h>
#include <SD.h>

File LDR;
File PIR;
File TEMP;
File MQ7;
File MQ6;
void setup() {
  // Open serial communications and wait for port to open:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }


  Serial.print("Initializing SD card...");

  if (!SD.begin(4)) {
    Serial.println("initialization failed!");
    return;
  }
  Serial.println("initialization done.");

}

void loop() {
  // nothing happens after setup
  int ldr = analogRead(A3);
  ldr = (1023-ldr);
  int pir = digitalRead(6);
  int temp = analogRead(A2);
  temp = 0.48*temp;
  int mq7 = analogRead(A1);//gas1
  int mq6 = analogRead(A0);//gas0
  LDR = SD.open("ldr.txt", FILE_WRITE);
  PIR = SD.open("pir.txt", FILE_WRITE);
  TEMP = SD.open("temp.txt", FILE_WRITE);
  MQ7 = SD.open("mq7.txt", FILE_WRITE);
  MQ6 = SD.open("mq6.txt", FILE_WRITE);
  delay(3000);
  // if the file opened okay, write to it:
  if (LDR || PIR || TEMP || MQ7 || MQ6) {
    Serial.println("Writing to test.txt...");
    //*********LDR READING****************//
    LDR.print("LDR:");
    LDR.println(ldr);
    Serial.print("LDR: ");
    Serial.println(ldr);
    //*********PIR READING****************//
    PIR.print("PIR:");
    PIR.println(pir);
    Serial.print("PIR: ");
    Serial.println(pir);
     //*********LM35 READING****************//
    TEMP.print("TEMP:");
    TEMP.println(temp);
    Serial.print("TEMP: ");
    Serial.println(temp);
     //*********MQ7 READING****************//
    MQ7.print("MQ7:");
    MQ7.println(mq7);
    Serial.print("MQ7: ");
    Serial.println(mq7);
   //*********MQ6 READING****************//
    MQ6.print("MQ6:");
    MQ6.println(mq6);
    Serial.print("MQ6: ");
    Serial.println(mq6);
    LDR.close();
    PIR.close();
    TEMP.close();
    MQ7.close();
    MQ6.close();
    Serial.println("done.");
  } 
  else {
    // if the file didn't open, print an error:
    Serial.println("error opening test.txt");
  }

}


